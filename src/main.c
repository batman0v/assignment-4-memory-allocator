#include "mem.h"
#include <assert.h>
#include <stdio.h>

int passed_tests = 0;
int total_tests = 5;

void run_test(const char* test_name, void (*test_function)()) {
    heap_init(0);
    printf("--> %s\n", test_name);
    test_function();
    heap_term();
    printf(" Success!\n");
    passed_tests++;
}

void check_memory_allocation_success() {
    void* mem1 = _malloc(100);
    assert(mem1 != NULL);
    _free(mem1);
}

void check_memory_freeing_single_block() {
    void* mem1 = _malloc(100);
    void* mem2 = _malloc(200);
    _free(mem1);
    _free(mem2);
}

void check_memory_freeing_double_block() {
    void* mem1 = _malloc(100);
    void* mem2 = _malloc(200);
    void* mem3 = _malloc(300);
    _free(mem1);
    _free(mem2);
    _free(mem3);
}

void check_expanded_memory_capacity() {
    void* mem1 = _malloc(100000);
    assert(mem1 != NULL);
    _free(mem1);
}

void check_memory_grow_within_address_limit() {
    void* mem1 = _malloc(100000);
    void* mem2 = _malloc(100000);
    assert(mem1 != NULL && mem2 != NULL);
    _free(mem1);
    void* mem3 = _malloc(200000);
    assert(mem3 != NULL);
    _free(mem2);
    _free(mem3);
}

int main() {
    run_test("TEST #1: Check memory allocation", check_memory_allocation_success);
    run_test("TEST #2: Check memory freeing single block", check_memory_freeing_single_block);
    run_test("TEST #3: Check memory freeing double block", check_memory_freeing_double_block);
    run_test("TEST #4: Check expanded memory capacity", check_expanded_memory_capacity);
    run_test("TEST #5: Check memory grow within address limit", check_memory_grow_within_address_limit);

    printf("\n!!! SUCCESS, %d/%d TESTS ARE PASSED !!!\n", passed_tests, total_tests);
    return 0;
}
